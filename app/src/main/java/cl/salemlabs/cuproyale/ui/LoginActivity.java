package cl.salemlabs.cuproyale.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import cl.salemlabs.cuproyale.R;
import cl.salemlabs.cuproyale.ui.model.UserViewModel;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity implements LoginContract.View{

  LoginContract.Presenter presenter;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Fabric.with(this, new Crashlytics());
    setContentView(R.layout.activity_login);

  }

  public void forceCrash(View view) {
  }



  @Override public void showProgresDialog() {

  }

  @Override public void showErrorMessage(String message) {

  }

  @Override public void loginOk(UserViewModel token) {

  }

  private class ExceptionHandler implements Thread.UncaughtExceptionHandler {
    public ExceptionHandler(LoginActivity loginActivity, Thread.UncaughtExceptionHandler mDefaultUEH) {

    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {

    }
  }
}
